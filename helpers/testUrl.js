function testUrl(url) {
    return new Promise((resolve, reject) => {
        const element = document.createElement('link');
        element.href = url;
        const id = 'rel-'+Math.random();
        element.id = id;
        element.rel = 'stylesheet';
        element.onerror = (event, err) => {
            console.log('onerror', event, err);
            element.remove();
            reject('blocked');
        }
        element.onabort = (e) => {
            console.log('onabort', e);
            element.remove();
            reject('blocked');
        }
        element.onload = (e) => {
            //console.log('onload', e);
            element.remove();
            resolve('success');
        }
        setTimeout(() => {
            element.remove();
            reject('error (timeout)');
        }, 10000);
        document.body.appendChild(element);
    });
}

export default testUrl;