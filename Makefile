install:
	docker compose run --rm npm bash -c "rm -rf ./node_modules && rm -rf ./.nuxt && rm -rf ./dist && yarn install"

build:
	docker compose run --rm npm bash -c "yarn build"

dev:
	docker compose up -d dev